FROM openjdk:11-jre
WORKDIR /app
ADD target/app.jar /app
CMD ["java", "-jar", "app.jar"]